data "aws_route53_zone" "zone" {
  name = var.domain
}

data "aws_acm_certificate" "certificate" {
  domain      = var.domain
  types       = ["AMAZON_ISSUED"]
  statuses    = ["ISSUED"]
  most_recent = true
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${module.bucket.s3.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }
}

# Populate on cloudflare conditionally
data "cloudflare_zones" "zone" {
  count  = var.cloudflare ? 1 : 0

  filter {
    name = var.domain
  }
}
