terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 4.13.0"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = ">= 3.14.0"
    }
  }
}

module "bucket" {
  source        = "git::https://gitlab.com/mesabg-tfmodules/bucket.git?ref=v2.0.3"
  environment   = var.environment
  name          = var.subdomain
  cors_enabled  = true
  public_access = var.public_access
}

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = var.subdomain
}

resource "aws_cloudfront_distribution" "cloudfront_distribution" {
  depends_on  = [module.bucket]

  origin {
    domain_name = module.bucket.s3.bucket_regional_domain_name
    origin_id   = "S3-${var.subdomain}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = "S3-${var.subdomain}"

    forwarded_values {
      query_string = true
      headers      = [
        "Origin",
        "Accept",
        "Accept-Encoding",
        "Accept-Language",
        "Access-Control-Request-Headers",
        "Access-Control-Request-Method",
        "Authorization",
        "Referer",
        "Cache-Control"
      ]

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = var.cache_enabled ? 3600 : 0
    max_ttl                = var.cache_enabled ? 86400 : 0
    compress               = true
    viewer_protocol_policy = var.viewer_protocol_policy
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.cache_behaviors
    iterator = behavior
    content {
      path_pattern     = behavior.value.path
      allowed_methods  = ["GET", "HEAD", "OPTIONS"]
      cached_methods   = ["GET", "HEAD", "OPTIONS"]
      target_origin_id = "S3-${var.subdomain}"

      forwarded_values {
        query_string = true
        headers      = [
          "Origin",
          "Accept",
          "Accept-Encoding",
          "Accept-Language",
          "Access-Control-Request-Headers",
          "Access-Control-Request-Method",
          "Authorization",
          "Referer",
          "Cache-Control"
        ]

        cookies {
          forward = "none"
        }
      }

      min_ttl                = behavior.value.min_ttl
      default_ttl            = behavior.value.default_ttl
      max_ttl                = behavior.value.max_ttl
      compress               = true
      viewer_protocol_policy = var.viewer_protocol_policy
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = var.name
  default_root_object = "${var.subdirectory != "" ? "${var.subdirectory}/" : ""}index.html"
  http_version        = "http2"
  wait_for_deployment = true

  aliases = [var.subdomain]

  price_class = "PriceClass_All"

  viewer_certificate {
    acm_certificate_arn             = data.aws_acm_certificate.certificate.arn
    cloudfront_default_certificate  = false
    minimum_protocol_version        = "TLSv1.1_2016"
    ssl_support_method              = "sni-only"
  }

  custom_error_response {
    error_caching_min_ttl   = 0
    error_code              = 403
    response_code           = 200
    response_page_path      = "${var.subdirectory}/index.html"
  }

  custom_error_response {
    error_caching_min_ttl   = 0
    error_code              = 404
    response_code           = 200
    response_page_path      = "${var.subdirectory}/index.html"
  }

  restrictions {
    geo_restriction {
      restriction_type      = "none"
    }
  }

  tags = {
    Name        = var.name
    Environment = var.environment
  }
}

resource "aws_s3_bucket_policy" "s3_bucket_policy" {
  depends_on  = [module.bucket]
  bucket      = module.bucket.s3.id
  policy      = data.aws_iam_policy_document.s3_policy.json
}

resource "aws_route53_record" "route53_record" {
  zone_id                   = data.aws_route53_zone.zone.zone_id
  name                      = var.subdomain
  type                      = "A"

  alias {
    name                    = aws_cloudfront_distribution.cloudfront_distribution.domain_name
    zone_id                 = aws_cloudfront_distribution.cloudfront_distribution.hosted_zone_id
    evaluate_target_health  = false
  }
}

resource "cloudflare_record" "record" {
  count     = var.cloudflare ? 1 : 0

  zone_id   = data.cloudflare_zones.zone[0].zones[0].id
  name      = var.subdomain
  content   = aws_cloudfront_distribution.cloudfront_distribution.domain_name
  type      = "CNAME"
  proxied   = false
}
