output "s3" {
  value       = module.bucket.s3
  description = "Created S3 Bucket"
}

output "cdn" {
  value       = aws_cloudfront_distribution.cloudfront_distribution
  description = "Created CDN Cloudfront Dstribution"
}

output "record" {
  value       = aws_route53_record.route53_record
  description = "Created Route53 Record"
}
